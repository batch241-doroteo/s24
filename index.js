// console.log('Hello')

// Mini Activity

/*
1. Using the ES6 update, get the cube of 8.
2.Print the result on the console with the message: `The cube of 8 is` + result
3. Use the template literal in printing out the message
*/

let cube = 8 ** 3;

console.log(`The cube of 8 is ${cube}`)

// MINI ACTIVITY
/*
Desctructive the address array
Print the values in the console: I live at 258 Washington Ave, California, 99011
*/
const address = ['258', 'Washington Ave NW', 'California', '90011']

const [number, avenue, state, zip] = address
console.log(`I live at ${number} ${avenue}, ${state}, ${zip}`)

// MINI ACTIVITY
const animal = {
	name: 'Lolong',
	species: 'Saltwater Crocodile',
	weight: '1075 kgs',
	measurements: '20ft 3 in'
}

const {name, species, weight, measurements} = animal
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurements}`)


// MINI ACTIVITY
/*
1. Loop through the numbers using forEach using arrow function
2. Print the numbers in the console
3. Use the .reduce operator on the numbers array 
4. Assign the result on a variable
5. Print the variable on the console.
*/

let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => console.log(number))
let reduceNumber = numbers.reduce((x, y) => x + y)
console.log(reduceNumber)

// MINI ACTIVITY
/*
1. Create a "dog" class
2. inside of the class "dog", have a name, age, breed
3. Instantiate a new dog clas and print in the console
4. Send a screenshot of the output on hang outs
*/

class dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
let dogs = new dog()
dogs.name = 'Yuki';
dogs.age = 2;
dogs.breed = 'Exotic Bulldog'
console.log(dogs)